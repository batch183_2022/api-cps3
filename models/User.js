const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
   firstName: {
        type: String,
        required: [true, "First name is required"]
    },
    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    email:{
        type: String,
        required: [true, "Email is required"]
    },
     password:{
        type: String,
        required: [true, "Password is required"]
    },
    createdOn:{
        type: Date,
        default: new Date()
    },

     isAdmin:{
        type: Boolean,
        default: false
    }
    
   /* order:[
            {
                userId: {
                type: String,
                required: [true, "User Id is required"]
                },
                productId: {
                    type: String,
                    required: [true, "Product Id is required"]
                },
                price: {
                    type: Number, 
                },
                quantity: {
                    type: Number, 
                },
                total: {
                    type: Number,
                },
                purchasedOn: {
                    type: Date,
                    default: new Date()
                },
                status: {
                    type: String,
                    default: "pending"
                }
            }
        ]*/

    })

module.exports = mongoose.model("User", userSchema)
