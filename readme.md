# Booking API Overview
## Initial Set-up
* Change the MongoDB Atlas database connection string in the backend to ensure connectivity to the correct personal database.

* Import the capstone2-villegas.postman_collection.json in your postman application.

## Store the following data in your database
### User credentials (User Registration)
* Admin User
	* firstName : "John"
	* lastName : "Smith"
	* email : "john@mail.com"
	* password : "john123"
	* isAdmin : true (default value: manually change this one in MongoDB)

* Customer User
	* firstName : "Jane"
	* lastName : "Doe"
	* email : "jane@mail.com"
	* password : "jane123"
	* isAdmin : false (default value)

### Product Creation (Product Request > Create a product)
* Product 1
	* name : "Oppo A83"
	* description : "Awesome Performance: Octa 2.4GHz Exynos 1280, Awesome Memory: 6GB RAM"
	* price : 9999
	* stocks : 50
	* createdOn : 2022-07-07T08:53:41.832+00:00

* Product 2
	* name : "Samsung Galaxy A31 | 6GB RAM +128GB ROM | MediaTek Helio P65"
	* description : "Samsung Galaxy A31 smartphone was launched on 24th March 2020."
	* price : 11990
	* stocks : 100
	* createdOn : 2022-07-07T08:53:41.832+00:00

### Booking API Features
* User Registration
* Retrieve all users
* User Authentication (Admin & Non-admin)
* User set as admin (Admin only)
* User update status (Admin only)
* Checkout (Non-admin only)
* Get my orders (Non-admin only)
* Get all orders (Admin only)
* Create products (Admin only)
* Get all active products (Admin & Non-admin)
* Get specific product (Admin & Non-admin)
* Update product info (Admin only)
* Archive products (Admin only)